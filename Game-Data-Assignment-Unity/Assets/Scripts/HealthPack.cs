﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

	bool used = false;

	SpriteRenderer renderer;

	void Awake()
	{
		renderer = GetComponent<SpriteRenderer>();
	}

	public void SetUsedState( bool value)
	{
		used = value;
		renderer.enabled = !value;
	}

	public bool GetUsedState()
	{
		return used;
	}

	void OnTriggerEnter2D(Collider2D target)
    {
		if (!used)
			SetUsedState(true);
    }
}
