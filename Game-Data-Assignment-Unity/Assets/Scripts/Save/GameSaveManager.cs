﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }
			
        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

		//construct usable file path, from unity's persistant data path 
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }


	//Add to our list of strings
    public void AddObject(string item) {
        saveItems.Add(item);
    }

    public void Save() {
        saveItems.Clear();

		//Find All objects that inherit from our base Save class
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
			//Add the JSON strings, from our saved objects to our list of save items
            saveItems.Add(saveableObject.Serialize());
        }

		//Open a file write stream, for our new .json save file
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
			//Write our JSON strings into the save file
			//each line is a seperate JSON object
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }


    public void Load() {
		//firstPlay lets us keep track of wether or not we pressed the load button to load the scene
        firstPlay = false;
		//clear old data
        saveItems.Clear();
		//Reload the scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		//Don't load data unless we are told to by the user
        if (firstPlay) return;

		//Load Data From File
        LoadSaveGameData();
		//Clear Saveable Objects
        DestroyAllSaveableObjectsInScene();
		//Create new Saveable Objects, with the data that we loaded from the file
        CreateGameObjects();
    }

    void LoadSaveGameData() {
		//Open a file read stream for our save file
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
			
            while (gameDataFileStream.Peek() >= 0) //Loop until end of file
			{
				//Read each line of file and add to our list
				//Removing any unnessasary white space
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    void DestroyAllSaveableObjectsInScene() {
		//Finds all objects that inherit our Save class and destroy them
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    void CreateGameObjects() {
		foreach (string saveItem in saveItems) { //Go through each line (JSON Object)

			//A sub string that we will use to find the staring point of our JSON object
			//the @ operator will ensure that we will ignore any special characters like " or \
            string pattern = @"""prefabName"":""";
			//Get the index of where our substring was found
            int patternIndex = saveItem.IndexOf(pattern);
			//Get the index in the string, right after our substring has occured and the 
			// first " to get the prefab name
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
			//Get the index of when the prefab name ends
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);

			//Create a string for the prefab name, using the indexes that we just found
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

			//Instaniate our prefab from the name that we saved
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
			//Call the deserialize function on our new object
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
