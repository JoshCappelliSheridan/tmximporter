﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    [Serializable] //Ensures that unity will serialize data of this class
	public class Data : BaseData { //Base Data contains any data that we want every object to save (prefab name)

		//Data we would like to save/load
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    void Awake() {
		//Get Tank Component
        tank = GetComponent<Tank>();
		//Initiliaze our save data
        data = new Data();
    }

    public override string Serialize() {
		//Populate our data class with information about our Tank
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
		//Convert our class to a string, formatted to JSON specifications
        jsonString = JsonUtility.ToJson(data);
		//return the string
        return (jsonString);
    }

    public override void Deserialize(string jsonData) {
		//Convert JSON string to our data class
		//Will overwrite any data we have in there
        JsonUtility.FromJsonOverwrite(jsonData, data);

		//Take information from our data class and apply it to our tank
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}