﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HealthPackSave : Save {

	[Serializable] 
	public class Data : BaseData { 
		public bool usedState;
		public Vector3 position;
		public Quaternion rotation;
		public Color spriteColor;
	}


	public Data data;
	HealthPack healthPack;
	SpriteRenderer spriteRenderer;

	void Awake()
	{
		healthPack = GetComponent<HealthPack>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public override string Serialize() {
		string jsonString = "";

		data.prefabName = prefabName;
		data.usedState = healthPack.GetUsedState();
		data.position = transform.position;
		data.rotation = transform.rotation;
		data.spriteColor = spriteRenderer.color;

		jsonString = JsonUtility.ToJson(data);
		return (jsonString);
	}

	public override void Deserialize(string jsonData) {
		JsonUtility.FromJsonOverwrite(jsonData, data);

		transform.position = data.position;
		transform.rotation = data.rotation;
		healthPack.SetUsedState( data.usedState );
		spriteRenderer.color = data.spriteColor;
	}
}
