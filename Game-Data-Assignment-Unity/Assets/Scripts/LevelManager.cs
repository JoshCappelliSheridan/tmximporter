﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;
using System.Text.RegularExpressions;

public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform healthPackHolder;

    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject healthPackPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> healthPackPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;
    public int numberOfLayers;
    public int numberOfObjectGroups;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public List<string> mapDataStrings;
    public List<List<int>> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {


        //Clean Up
        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(healthPackHolder);

        // Get Path for TMX file
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // Read Map Data
        {
            //Clear any previous map data
            mapData = new List<List<int>>();

            //Get text in file
            string content = File.ReadAllText(TMXFile);

            numberOfLayers = Regex.Matches(content, "<layer").Count;
            numberOfObjectGroups = Regex.Matches(content, "<objectgroup").Count;

            //Create XML Reader Stream
            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                //Get Width and Height of Map
                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));


                //Get Tile Width and Height
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                //Get Sprite Sheet information (Tile Count, columns, rows)
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //Get Image Path
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //Clear Old Data
                turretPositions.Clear();
                mapData = new List<List<int>>();
                mapDataStrings = new List<string>();

                //Read normal Tile Layer Data
                for  (int i = 0; i < numberOfLayers; i++)
                {
                    if (reader.ReadToFollowing("layer"))
                        TranslateMapDataLayer(reader);
                }

                //Clear Old Data
                turretPositions = new List<Vector3>();
                healthPackPositions = new List<Vector3>(); 

                //Read Object Data For Turrets
                for (int i = 0; i < numberOfObjectGroups; i++)
                {
                    if (reader.ReadToFollowing("objectgroup"))
                    {
                        string groupName = reader.GetAttribute("name");

                        switch (groupName)
                        {
                            case "Enemies":
                                ReadObjectGroupData(turretPositions, reader);
                                break;
                            case "HealthPacks":
                                ReadObjectGroupData(healthPackPositions, reader);
                                break;
                            default:
                                Debug.LogError(groupName + " is an unknown object type");
                                break;
                        }
                    }
                }
            }

        }

        //Set up sizing and coordinates used to place objects
        {
            //Convert from pixel sizes to Unity World Size
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            //Set Map Origin in Unity World Space
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }


        // Import Sprite Sheet Texture
        {
            //Create Blank Texture
            spriteSheetTexture = new Texture2D(2, 2);
            //Read File
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            //Set Scaling For Pixel Art
            spriteSheetTexture.filterMode = FilterMode.Point;
            //Non-Tileable
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Create Sprites from Texture
        {
            //Clear Old Data
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    //Create Sprite Form Sprite Shiiet Texture
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, 
                        tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    //Add To Map Sprites List
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Create Tile GameObjects
        {
            //Clear Old Data
            tiles.Clear();

            //Create Map Layers
            for (int i = 0; i < mapData.Count; i++)
            {
                CreateLayer(mapData[i], i);
            }
        }


        // Create Turret Objects
        {
            foreach (Vector3 turretPosition in turretPositions) {
                //Instatiate Turret Prefab Based on Turret Positions pulled from TMX
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                //Name Turret
                turret.name = "Turret";
                //Parent To ""turretHolder"
                turret.transform.parent = turretHolder;
            }
        }

        // Create Health Pack Objects
        {
            foreach (Vector3 healthPackPosition in healthPackPositions)
            {
                //Instatiate Health Pack Prefab Based on Turret Positions pulled from TMX
                GameObject turret = Instantiate(healthPackPrefab, healthPackPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                //Name Health Pack
                turret.name = "Health Pack";
                //Parent To "healthPackHolder"
                turret.transform.parent = healthPackHolder;
            }
        }

        //Print Time Loaded
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }

    void TranslateMapDataLayer(XmlReader currentReader)
    {
        //Get Data
        currentReader.ReadToFollowing("data");

        //Determine Data Encoding Type
        SetEncodingType(currentReader.GetAttribute("encoding"));

        //LoadData and Trim Whitespace
        mapDataStrings.Add(currentReader.ReadElementContentAsString().Trim());

        TranslateMapDataStringToTileIDs(mapDataStrings[mapDataStrings.Count - 1]);
    }

    void TranslateMapDataStringToTileIDs(string mapDataString)
    {
        mapData.Add(new List<int>());
        List<int> idList = mapData[mapData.Count - 1];
        
        //Interpret Map Data Based on Type of Encoding
        switch (mapDataFormat)
        {

            case MapDataFormat.Base64:
                //Get Byte Data
                byte[] bytes = Convert.FromBase64String(mapDataString);
                int index = 0;
                while (index < bytes.Length)
                {
                    //Convert Bytes to Int
                    int tileID = BitConverter.ToInt32(bytes, index) - 1;
                    //Add To Map Data List
                    idList.Add(tileID);
                    index += 4;
                }
                break;


            case MapDataFormat.CSV:

                //Seperate Map Data By Row
                string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);

                //Interpret Data Row By Row
                foreach (string line in lines)
                {
                    //Get individual tile numbers and put them in map data
                    string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                    foreach (string value in values)
                    {
                        int tileID = Convert.ToInt32(value) - 1;
                        idList.Add(tileID);
                    }
                }
                break;
        }
    }

    void CreateLayer(List<int> layerData, int layerNumber)
    {
        // Loop Through Map Tiles
        for (int y = 0; y < mapRows; y++)
        {
            for (int x = 0; x < mapColumns; x++)
            {

                int mapDatatIndex = x + (y * mapColumns); // 2D Coordinate to 1D array
                int tileID = layerData[mapDatatIndex]; //Retrieve Tile ID

                if (tileID >= 0)// less then 0 means there is no tile
                {
                    //Create Object
                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;

                    //Set Sprite
                    SpriteRenderer spriteRenderer = tile.transform.GetChild(0).GetComponent<SpriteRenderer>();
                    spriteRenderer.sprite = mapSprites[tileID];
                    spriteRenderer.sortingOrder = layerNumber;

                    //Parent to "groundHolder" to keep make it easy to manipulate and find all map tiles
                    tile.transform.parent = groundHolder;

                    //Add to list
                    tiles.Add(tile);
                }
            }
        }
    }

    void SetEncodingType(string encodingAttribute)
    {
        switch (encodingAttribute)
        {
            case "base64":
                mapDataFormat = MapDataFormat.Base64;
                break;
            case "csv":
                mapDataFormat = MapDataFormat.CSV;
                break;
            default:
                Debug.LogError(encodingAttribute +  " is  NOT a supported encoding type");
                break;
        }
    }

    void ReadObjectGroupData(List<Vector3> positionStorage, XmlReader currentReader)
    {
        if (currentReader.ReadToDescendant("object"))
        {
            do
            {
                float x = Convert.ToSingle(currentReader.GetAttribute("x")) / (float)pixelsPerUnit;
                float y = Convert.ToSingle(currentReader.GetAttribute("y")) / (float)pixelsPerUnit;
                positionStorage.Add(new Vector3(x, -y, 0));
            } while (currentReader.ReadToNextSibling("object"));
        }
    }
}


